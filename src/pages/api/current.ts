import type { NextApiRequest, NextApiResponse } from 'next'

const current = {
	"gameProfileId": "6627ae87-6917-4a7e-82a4-905e592207db",
	"skinHexColor": "#fdfcfc",
	"parts": [
		{
			"gameProfileId": "6627ae87-6917-4a7e-82a4-905e592207db",
			"skinPartId": "88f8ad81-9df1-4882-ad60-100d83397071",
			"categoryId": "cc03418d-4e45-4740-9112-ccad1d8dd925",
			"category": "Asas",
			"skinType": "Skin",
			"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Yueh_cc03418d-4e45-4740-9112-ccad1d8dd925_20222414212444.png",
			"spineLibrary": "FullBody/Yueh",
			"spineParts": [
				"Wing-left",
				"Wing-right"
			]
		},
		{
			"gameProfileId": "6627ae87-6917-4a7e-82a4-905e592207db",
			"skinPartId": "857ec689-b46e-4d82-bdc2-65b41a03055b",
			"categoryId": "acceba58-717d-4c0c-8737-d680882011b8",
			"category": "Cauda",
			"skinType": "Skin",
			"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Yueh_acceba58-717d-4c0c-8737-d680882011b8_20222614212639.png",
			"spineLibrary": "FullBody/Yueh",
			"spineParts": [
				"Tail1",
				"Tail2"
			]
		}
	]
}

export default function handler(req: NextApiRequest, res: NextApiResponse) {
    res.status(200).json({ current })
}