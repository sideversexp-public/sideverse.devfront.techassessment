import type { NextApiRequest, NextApiResponse } from 'next'

const catalog = [
	{
		"categoryId": "cc03418d-4e45-4740-9112-ccad1d8dd925",
		"category": "Asas",
		"skinType": "Skin",
		"skinParts": [
			{
				"id": "88f8ad81-9df1-4882-ad60-100d83397071",
				"name": "Yueh",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Yueh_cc03418d-4e45-4740-9112-ccad1d8dd925_20222414212444.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Yueh"
			}
		],
		"spineParts": [
			"Wing-left",
			"Wing-right"
		]
	},
	{
		"categoryId": "d6ec4085-0e1a-4efe-a8c4-306d39d98382",
		"category": "Boca",
		"skinType": "Skin",
		"skinParts": [
			{
				"id": "dd88a94d-7292-4f1c-ae68-2374046b7212",
				"name": "Yueh",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Yueh_d6ec4085-0e1a-4efe-a8c4-306d39d98382_20222114212134.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Yueh"
			},
			{
				"id": "d948b069-a141-4bbd-869b-72231db7e46b",
				"name": "Esquimó",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Esquimo_d6ec4085-0e1a-4efe-a8c4-306d39d98382_2022_09_08_10_32_48_20223208223248.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Esquimo"
			},
			{
				"id": "fcde1a31-168a-4bd7-94c7-b1c7d11d89a4",
				"name": "Chinesa",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Chinesa_d6ec4085-0e1a-4efe-a8c4-306d39d98382_2022_09_08_10_21_10_20222108222111.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Chinesa"
			}
		],
		"spineParts": [
			"Mouth"
		]
	},
	{
		"categoryId": "6f2bace2-07f1-4024-9069-39ddec57b2d5",
		"category": "Chapéu",
		"skinType": "Skin",
		"skinParts": [
			{
				"id": "c04da44e-9e0b-420d-a9f3-2748293b9311",
				"name": "Chinesa",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Chinesa_6f2bace2-07f1-4024-9069-39ddec57b2d5_2022_09_08_10_22_23_20222208222223.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Chinesa"
			},
			{
				"id": "eea3081f-ae58-4998-a7f4-38970a1c0c9c",
				"name": "Yueh",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Yueh_6f2bace2-07f1-4024-9069-39ddec57b2d5_20222114212158.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Yueh"
			},
			{
				"id": "13465069-8e3d-48b3-9376-6190aea6c5a1",
				"name": "Esquimó",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Esquimo_6f2bace2-07f1-4024-9069-39ddec57b2d5_2022_09_08_10_33_17_20223308223317.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Esquimo"
			}
		],
		"spineParts": [
			"Hat"
		]
	},
	{
		"categoryId": "2d0d2194-5ef1-4bac-860d-0244f83d86db",
		"category": "Acessório Frente",
		"skinType": "Skin",
		"skinParts": [
			{
				"id": "a76bddbc-1778-4350-b366-2765dea7c545",
				"name": "Yueh",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Yueh_2d0d2194-5ef1-4bac-860d-0244f83d86db_20222714212732.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Yueh"
			}
		],
		"spineParts": [
			"Acessorio-front"
		]
	},
	{
		"categoryId": "a5997390-8f16-4e69-95f5-8c3dea6c05fe",
		"category": "Braço",
		"skinType": "Skin",
		"skinParts": [
			{
				"id": "5a9d5c16-6056-439f-81a9-2e10299e7f79",
				"name": "Esquimó",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Esquimo_a5997390-8f16-4e69-95f5-8c3dea6c05fe_2022_09_08_10_34_18_20223408223418.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Esquimo"
			},
			{
				"id": "bf677014-760b-467f-8eb2-86e6dad5eafb",
				"name": "Yueh",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Yueh_a5997390-8f16-4e69-95f5-8c3dea6c05fe_20222314212351.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Yueh"
			}
		],
		"spineParts": [
			"Arm-left",
			"Arm-right",
			"Forearm-left",
			"Forearm-right"
		]
	},
	{
		"categoryId": "ecbd9491-3c43-47ec-98f1-d450afecde83",
		"category": "Nariz",
		"skinType": "Skin",
		"skinParts": [
			{
				"id": "4426baed-0886-486c-9d6c-369d4d3513e6",
				"name": "Esquimó",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Esquimo_ecbd9491-3c43-47ec-98f1-d450afecde83_2022_09_08_10_35_43_20223508223543.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Esquimo"
			},
			{
				"id": "03037e4a-5a1f-4638-b9ba-4b703e7441ae",
				"name": "Chinesa",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Chinesa_ecbd9491-3c43-47ec-98f1-d450afecde83_2022_09_08_10_24_06_20222408222406.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Chinesa"
			},
			{
				"id": "5186be31-b300-46b7-a653-c6b9bfc187c3",
				"name": "Yueh",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Yueh_ecbd9491-3c43-47ec-98f1-d450afecde83_20222514212554.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Yueh"
			}
		],
		"spineParts": [
			"Nose"
		]
	},
	{
		"categoryId": "d419454a-a5e9-40ab-90ba-fe45c365c255",
		"category": "Olhos",
		"skinType": "Skin",
		"skinParts": [
			{
				"id": "ba48439c-6c33-4ddd-b131-3d04932a5348",
				"name": "Chinesa",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Chinesa_d419454a-a5e9-40ab-90ba-fe45c365c255_2022_09_08_10_25_02_20222508222502.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Chinesa"
			},
			{
				"id": "0750ce38-77e5-4712-afd5-7ae588090f24",
				"name": "Yueh",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Yueh_d419454a-a5e9-40ab-90ba-fe45c365c255_20222714212748.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Yueh"
			},
			{
				"id": "65963380-807d-410f-a55b-a9c01424f1c2",
				"name": "Esquimó",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Esquimo_d419454a-a5e9-40ab-90ba-fe45c365c255_2022_09_08_10_36_52_20223608223652.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Esquimo"
			}
		],
		"spineParts": [
			"Eye",
			"Eyebrow"
		]
	},
	{
		"categoryId": "806a6a00-95b6-435b-a38b-cee409a9ed1e",
		"category": "Cabelo",
		"skinType": "Skin",
		"skinParts": [
			{
				"id": "6d788c91-388e-4d20-991f-44cf6f6d69e0",
				"name": "Yueh",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Yueh_806a6a00-95b6-435b-a38b-cee409a9ed1e_20222514212523.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Yueh"
			},
			{
				"id": "a94c5f4e-6b5e-479c-8b87-45ba619d7287",
				"name": "Esquimó",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Esquimo_806a6a00-95b6-435b-a38b-cee409a9ed1e_2022_09_08_10_35_05_20223508223506.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Esquimo"
			},
			{
				"id": "911699e7-003e-4fe4-8eba-4b67ee068567",
				"name": "Chinesa",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Chinesa_806a6a00-95b6-435b-a38b-cee409a9ed1e_2022_09_08_10_23_27_20222308222328.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Chinesa"
			}
		],
		"spineParts": [
			"Hair-back",
			"Hair-front"
		]
	},
	{
		"categoryId": "792e738c-1d49-4853-bf84-1f15a50bbb8e",
		"category": "Corpo",
		"skinType": "Skin",
		"skinParts": [
			{
				"id": "6b46b182-ceeb-4214-b649-48cf7a3a6a46",
				"name": "Yueh",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Yueh_792e738c-1d49-4853-bf84-1f15a50bbb8e_20222014212012.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Yueh"
			},
			{
				"id": "a8fc860e-4422-41ac-97c0-59f9046f1cf0",
				"name": "Esquimó",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Esquimo_792e738c-1d49-4853-bf84-1f15a50bbb8e_2022_09_08_10_31_47_20223108223148.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Esquimo"
			},
			{
				"id": "059b3f05-12e2-4726-a923-fd7c4dab8434",
				"name": "Chinesa",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Chinesa_792e738c-1d49-4853-bf84-1f15a50bbb8e_2022_09_08_09_51_00_20225108215108.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Chinesa"
			}
		],
		"spineParts": [
			"Torso"
		]
	},
	{
		"categoryId": "43a137d8-9822-4604-8b72-46d75baedd29",
		"category": "Pernas",
		"skinType": "Skin",
		"skinParts": [
			{
				"id": "2d7ca1fe-1a4b-4811-8eec-4ab43d584d71",
				"name": "Yueh",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Yueh_43a137d8-9822-4604-8b72-46d75baedd29_20222214212218.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Yueh"
			},
			{
				"id": "d4e175e8-2e09-4731-870f-ce0c1473d73b",
				"name": "Chinesa",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Chinesa_43a137d8-9822-4604-8b72-46d75baedd29_2022_09_08_10_22_54_20222208222254.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Chinesa"
			},
			{
				"id": "9ee3f160-5657-4f07-81ed-d732fdf3070c",
				"name": "Esquimó",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Esquimo_43a137d8-9822-4604-8b72-46d75baedd29_2022_09_08_10_33_48_20223308223348.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Esquimo"
			}
		],
		"spineParts": [
			"Foot-left",
			"Foot-right",
			"Leg-left",
			"Leg-right",
			"Thigh-left",
			"Thigh-right"
		]
	},
	{
		"categoryId": "acceba58-717d-4c0c-8737-d680882011b8",
		"category": "Cauda",
		"skinType": "Skin",
		"skinParts": [
			{
				"id": "857ec689-b46e-4d82-bdc2-65b41a03055b",
				"name": "Yueh",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Yueh_acceba58-717d-4c0c-8737-d680882011b8_20222614212639.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Yueh"
			}
		],
		"spineParts": [
			"Tail1",
			"Tail2"
		]
	},
	{
		"categoryId": "033513ed-092d-4863-9fe2-240d21a0a2dd",
		"category": "Halo",
		"skinType": "Skin",
		"skinParts": [
			{
				"id": "83761b5c-609a-4b49-88da-6b70814027f6",
				"name": "Yueh",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Yueh_033513ed-092d-4863-9fe2-240d21a0a2dd_20222014212046.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Yueh"
			}
		],
		"spineParts": [
			"Halo"
		]
	},
	{
		"categoryId": "a711b452-dee3-4e30-aba8-77ebda4ad95b",
		"category": "Acessório Costas",
		"skinType": "Skin",
		"skinParts": [
			{
				"id": "7c0e6495-7686-4a9f-9aaa-9db9ce8f22b8",
				"name": "Yueh",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Yueh_a711b452-dee3-4e30-aba8-77ebda4ad95b_20222314212325.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Yueh"
			}
		],
		"spineParts": [
			"Acessorio-back"
		]
	},
	{
		"categoryId": "a7cf7028-2d19-449a-853d-0aecc3fc3636",
		"category": "Calças",
		"skinType": "Skin",
		"skinParts": [
			{
				"id": "e3c96df6-8e3e-4114-a761-b0aa1def7d58",
				"name": "Esquimó",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Esquimo_a7cf7028-2d19-449a-853d-0aecc3fc3636_2022_09_08_10_31_16_20223108223116.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Esquimo"
			},
			{
				"id": "454c96fb-3bcb-484f-b008-fdd4702fdfd4",
				"name": "Yueh",
				"previewImageUrl": "https://educaversoassetsdev.blob.core.windows.net/assets/bosses/Yueh_a7cf7028-2d19-449a-853d-0aecc3fc3636_20221914211937.png",
				"isUnlocked": true,
				"spineLibrary": "FullBody/Yueh"
			}
		],
		"spineParts": [
			"Pelvis"
		]
	}
]

export default function handler(req: NextApiRequest, res: NextApiResponse) {
    res.status(200).json({ catalog })
}