/******************************************************************************
 * Spine Runtimes License Agreement
 * Last updated September 24, 2021. Replaces all prior versions.
 *
 * Copyright (c) 2013-2021, Esoteric Software LLC
 *
 * Integration of the Spine Runtimes into software or otherwise creating
 * derivative works of the Spine Runtimes is permitted under the terms and
 * conditions of Section 2 of the Spine Editor License Agreement:
 * http://esotericsoftware.com/spine-editor-license
 *
 * Otherwise, it is permitted to integrate the Spine Runtimes into software
 * or otherwise create derivative works of the Spine Runtimes (collectively,
 * "Products"), provided that each user of the Products must obtain their own
 * Spine Editor license and redistribution of the Products in any form must
 * include this license and copyright notice.
 *
 * THE SPINE RUNTIMES ARE PROVIDED BY ESOTERIC SOFTWARE LLC "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL ESOTERIC SOFTWARE LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES,
 * BUSINESS INTERRUPTION, OR LOSS OF USE, DATA, OR PROFITS) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THE SPINE RUNTIMES, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/
import { BlendMode } from "@esotericsoftware/spine-core";
export class ManagedWebGLRenderingContext {
    constructor(canvasOrContext, contextConfig = { alpha: "true" }) {
        this.restorables = new Array();
        if (!((canvasOrContext instanceof WebGLRenderingContext) || (typeof WebGL2RenderingContext !== 'undefined' && canvasOrContext instanceof WebGL2RenderingContext))) {
            let canvas = canvasOrContext;
            this.gl = (canvas.getContext("webgl2", contextConfig) || canvas.getContext("webgl", contextConfig));
            this.canvas = canvas;
            canvas.addEventListener("webglcontextlost", (e) => {
                let event = e;
                if (e)
                    e.preventDefault();
            });
            canvas.addEventListener("webglcontextrestored", (e) => {
                for (let i = 0, n = this.restorables.length; i < n; i++)
                    this.restorables[i].restore();
            });
        }
        else {
            this.gl = canvasOrContext;
            this.canvas = this.gl.canvas;
        }
    }
    addRestorable(restorable) {
        this.restorables.push(restorable);
    }
    removeRestorable(restorable) {
        let index = this.restorables.indexOf(restorable);
        if (index > -1)
            this.restorables.splice(index, 1);
    }
}
const ONE = 1;
const ONE_MINUS_SRC_COLOR = 0x0301;
const SRC_ALPHA = 0x0302;
const ONE_MINUS_SRC_ALPHA = 0x0303;
const ONE_MINUS_DST_ALPHA = 0x0305;
const DST_COLOR = 0x0306;
export class WebGLBlendModeConverter {
    static getDestGLBlendMode(blendMode) {
        switch (blendMode) {
            case BlendMode.Normal: return ONE_MINUS_SRC_ALPHA;
            case BlendMode.Additive: return ONE;
            case BlendMode.Multiply: return ONE_MINUS_SRC_ALPHA;
            case BlendMode.Screen: return ONE_MINUS_SRC_ALPHA;
            default: throw new Error("Unknown blend mode: " + blendMode);
        }
    }
    static getSourceColorGLBlendMode(blendMode, premultipliedAlpha = false) {
        switch (blendMode) {
            case BlendMode.Normal: return premultipliedAlpha ? ONE : SRC_ALPHA;
            case BlendMode.Additive: return premultipliedAlpha ? ONE : SRC_ALPHA;
            case BlendMode.Multiply: return DST_COLOR;
            case BlendMode.Screen: return ONE;
            default: throw new Error("Unknown blend mode: " + blendMode);
        }
    }
    static getSourceAlphaGLBlendMode(blendMode) {
        switch (blendMode) {
            case BlendMode.Normal: return ONE;
            case BlendMode.Additive: return ONE;
            case BlendMode.Multiply: return ONE_MINUS_SRC_ALPHA;
            case BlendMode.Screen: return ONE_MINUS_SRC_COLOR;
            default: throw new Error("Unknown blend mode: " + blendMode);
        }
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiV2ViR0wuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvV2ViR0wudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzsrRUEyQitFO0FBRS9FLE9BQU8sRUFBYyxTQUFTLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUVyRSxNQUFNLE9BQU8sNEJBQTRCO0lBS3hDLFlBQWEsZUFBMEQsRUFBRSxnQkFBcUIsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFO1FBRnZHLGdCQUFXLEdBQUcsSUFBSSxLQUFLLEVBQWMsQ0FBQztRQUc3QyxJQUFJLENBQUMsQ0FBQyxDQUFDLGVBQWUsWUFBWSxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxzQkFBc0IsS0FBSyxXQUFXLElBQUksZUFBZSxZQUFZLHNCQUFzQixDQUFDLENBQUMsRUFBRTtZQUNsSyxJQUFJLE1BQU0sR0FBc0IsZUFBZSxDQUFDO1lBQ2hELElBQUksQ0FBQyxFQUFFLEdBQTBCLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsYUFBYSxDQUFDLElBQUksTUFBTSxDQUFDLFVBQVUsQ0FBQyxPQUFPLEVBQUUsYUFBYSxDQUFDLENBQUMsQ0FBQztZQUMzSCxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztZQUNyQixNQUFNLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxDQUFNLEVBQUUsRUFBRTtnQkFDdEQsSUFBSSxLQUFLLEdBQXNCLENBQUMsQ0FBQztnQkFDakMsSUFBSSxDQUFDO29CQUFFLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUMzQixDQUFDLENBQUMsQ0FBQztZQUNILE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDLENBQU0sRUFBRSxFQUFFO2dCQUMxRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUU7b0JBQ3RELElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDaEMsQ0FBQyxDQUFDLENBQUM7U0FDSDthQUFNO1lBQ04sSUFBSSxDQUFDLEVBQUUsR0FBRyxlQUFlLENBQUM7WUFDMUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQztTQUM3QjtJQUNGLENBQUM7SUFFRCxhQUFhLENBQUUsVUFBc0I7UUFDcEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVELGdCQUFnQixDQUFFLFVBQXNCO1FBQ3ZDLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ2pELElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQztZQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNuRCxDQUFDO0NBQ0Q7QUFFRCxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUM7QUFDZCxNQUFNLG1CQUFtQixHQUFHLE1BQU0sQ0FBQztBQUNuQyxNQUFNLFNBQVMsR0FBRyxNQUFNLENBQUM7QUFDekIsTUFBTSxtQkFBbUIsR0FBRyxNQUFNLENBQUM7QUFDbkMsTUFBTSxtQkFBbUIsR0FBRyxNQUFNLENBQUM7QUFDbkMsTUFBTSxTQUFTLEdBQUcsTUFBTSxDQUFDO0FBRXpCLE1BQU0sT0FBTyx1QkFBdUI7SUFDbkMsTUFBTSxDQUFDLGtCQUFrQixDQUFFLFNBQW9CO1FBQzlDLFFBQVEsU0FBUyxFQUFFO1lBQ2xCLEtBQUssU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE9BQU8sbUJBQW1CLENBQUM7WUFDbEQsS0FBSyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxHQUFHLENBQUM7WUFDcEMsS0FBSyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxtQkFBbUIsQ0FBQztZQUNwRCxLQUFLLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxPQUFPLG1CQUFtQixDQUFDO1lBQ2xELE9BQU8sQ0FBQyxDQUFDLE1BQU0sSUFBSSxLQUFLLENBQUMsc0JBQXNCLEdBQUcsU0FBUyxDQUFDLENBQUM7U0FDN0Q7SUFDRixDQUFDO0lBRUQsTUFBTSxDQUFDLHlCQUF5QixDQUFFLFNBQW9CLEVBQUUscUJBQThCLEtBQUs7UUFDMUYsUUFBUSxTQUFTLEVBQUU7WUFDbEIsS0FBSyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsT0FBTyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7WUFDbkUsS0FBSyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7WUFDckUsS0FBSyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxTQUFTLENBQUM7WUFDMUMsS0FBSyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsT0FBTyxHQUFHLENBQUM7WUFDbEMsT0FBTyxDQUFDLENBQUMsTUFBTSxJQUFJLEtBQUssQ0FBQyxzQkFBc0IsR0FBRyxTQUFTLENBQUMsQ0FBQztTQUM3RDtJQUNGLENBQUM7SUFFRCxNQUFNLENBQUMseUJBQXlCLENBQUUsU0FBb0I7UUFDckQsUUFBUSxTQUFTLEVBQUU7WUFDbEIsS0FBSyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsT0FBTyxHQUFHLENBQUM7WUFDbEMsS0FBSyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxHQUFHLENBQUM7WUFDcEMsS0FBSyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxtQkFBbUIsQ0FBQztZQUNwRCxLQUFLLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxPQUFPLG1CQUFtQixDQUFDO1lBQ2xELE9BQU8sQ0FBQyxDQUFDLE1BQU0sSUFBSSxLQUFLLENBQUMsc0JBQXNCLEdBQUcsU0FBUyxDQUFDLENBQUM7U0FDN0Q7SUFDRixDQUFDO0NBQ0QifQ==